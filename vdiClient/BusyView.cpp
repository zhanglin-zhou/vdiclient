// BusyView.cpp : implementation file
//

#include "stdafx.h"
#include "vdiClient.h"
#include "BusyView.h"


// CBusyView

IMPLEMENT_DYNAMIC(CBusyView, CWnd)

CBusyView::CBusyView()
{

}

CBusyView::~CBusyView()
{
}


BEGIN_MESSAGE_MAP(CBusyView, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CBusyView::SetBusy(LPCTSTR text, CWnd *previousView)
{
	m_busyText = text;
	if (previousView != NULL) {
		m_previousView = previousView;
	}
}

CWnd* CBusyView::CleanBusy()
{
	ShowWindow(SW_HIDE);
	m_busyText.Empty();
	return m_previousView;
}



// CBusyView message handlers


void CBusyView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rect;
	GetClientRect(rect);
	dc.DrawText(m_busyText, rect, DT_SINGLELINE| DT_VCENTER | DT_CENTER | DT_NOPREFIX);
}

BOOL CBusyView::PreCreateWindow(CREATESTRUCT& cs)
{

	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;

}
