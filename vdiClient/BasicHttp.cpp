#include "stdafx.h"
#include "BasicHttp.h"
#include <curl\curl.h>
#include <string.h>

static const char *defaultUserAgent = "wisvdi-client";
static const char *SESSIONID = "JSESSIONID";

BasicHttpRequest::BasicHttpRequest(void)
	:m_userAgent(defaultUserAgent),
	 m_body("")
{
}


BasicHttpRequest::~BasicHttpRequest(void)
{
}


void BasicHttpRequest::setUrl(const char* url)
{
	m_url = url;
}


int BasicHttpRequest::perform(int httpMethod)
{
	CURLcode res = CURLE_OK;
	CURL  *curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl,  CURLOPT_ENCODING, "UTF-8");
		curl_easy_setopt(curl, CURLOPT_URL, m_url.c_str());
		//follow redirection
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		curl_easy_setopt(curl, CURLOPT_USERAGENT, m_userAgent.c_str());
		if (m_cookie.length() > 0) {
			curl_easy_setopt(curl, CURLOPT_COOKIE, m_cookie.c_str());
		}

		switch (httpMethod) {
		case 0:
			curl_easy_setopt(curl, CURLOPT_HTTPGET, (long) 1);
			break;

		case 1:
			curl_easy_setopt(curl, CURLOPT_POST, (long) 1);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, m_body.c_str());
			curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long) m_body.length());
			break;
		default:
			// TODO: add later
			break;
		}

		if (m_headers.size()>0) {
			struct curl_slist *headers = NULL;
			for (list<string>::const_iterator it = m_headers.begin(); it != m_headers.end(); it++) {
				curl_slist_append(headers, it->c_str());
			}
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		}
		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);
		curl_easy_setopt(curl, CURLOPT_HEADERDATA, this);

		/* the reply */ 
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, this); 


		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &m_responseCode);
		/* Check for errors */
		if(res != CURLE_OK) {
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));
			TRACE("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		}
		curl_easy_cleanup(curl);
	}
	return res;
}


void BasicHttpRequest::addHeader(const char* header)
{
	m_headers.push_back(header);
}


void BasicHttpRequest::setBody(const char* body)
{
	m_body = body;
}

void BasicHttpRequest::setCookie(const char* cookie)
{
	m_cookie = cookie;
}

int BasicHttpRequest::doGet(void)
{
	return perform(0);
}


int BasicHttpRequest::doPost(void)
{
	return perform(1);
}

long BasicHttpRequest::getResponseCode(void)
{
	return m_responseCode;
}

const char* BasicHttpRequest::getResponse()
{
	return m_response.c_str();
}

void BasicHttpRequest::setResponse( const char* response )
{
	m_response = response;
}

size_t BasicHttpRequest::header_callback( char *buffer, size_t size, size_t nitems, void *userdata )
{
	BasicHttpRequest *request = (BasicHttpRequest*) userdata;
	ASSERT(request != NULL);
	size_t bufferSize = size * nitems;
	if (bufferSize == 0) {
		return bufferSize;
	}
	if (strncmp(buffer, "Set-Cookie:", 11) == 0) {
		char* buf = strstr(buffer, SESSIONID);
		if (buf != NULL) {
			char* cookie = (char*) malloc(bufferSize);
			sscanf(buf, "%[^;]", cookie);
			request->setCookie(cookie);
			free(cookie);
		}
	}
	return bufferSize;
}


size_t BasicHttpRequest::write_callback( char *ptr, size_t size, size_t nmemb, void *userdata )
{
	BasicHttpRequest *request = (BasicHttpRequest*) userdata;
	ASSERT(request != NULL);
	size_t bufferSize = size * nmemb;
	if (bufferSize == 0) {
		return bufferSize;
	}
	request->setResponse(string(ptr, bufferSize).c_str());
	return bufferSize;
}




