﻿// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Message.h"
#include "vdiClient.h"
#include "MainFrm.h"
#include "LoginDialog.h"
#include <Wtsapi32.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern BOOL gEnableAppExit;
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_AUTHENTICATE, OnAuthenticate)
	ON_MESSAGE(WM_BROKERCONNECT, OnBrokerConnect)
	ON_MESSAGE(WM_BROKERDISCONNECTED, OnBrokerDisconnected)
	ON_MESSAGE(WM_LAUNCHITEMUPDATED, OnLaunchItemUpdated)
	ON_MESSAGE(WM_LAUNCHITEMCLICKED, OnLaunchItemClicked)
	ON_MESSAGE(WM_LAUNCHITMECONNECT, OnLaunchItemConnect)
	ON_MESSAGE(WM_LAUNCHITMEFAILED, OnLaunchItemFailed)

	ON_COMMAND(ID_DISCONNECT, &CMainFrame::OnDisconnect)
	ON_UPDATE_COMMAND_UI(ID_DISCONNECT, &CMainFrame::OnUpdateDisconnect)
	ON_UPDATE_COMMAND_UI(ID_APP_EXIT, &CMainFrame::OnUpdateAppExit)
	ON_WM_CLOSE()
//	ON_WM_NCHITTEST()
//	ON_WM_GETMINMAXINFO()
//	ON_WM_MOVE()
	ON_WM_MOVING()
//ON_WM_SIZING()
//ON_WM_SIZE()
ON_WM_SYSCOMMAND()
ON_WM_WTSSESSION_CHANGE()
ON_WM_DESTROY()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
	:m_isLogin(FALSE),
	 m_currentBroker(NULL)
{
	// TODO: add member initialization code here
	m_brokerView = new CBrokerView();
	m_selectorView = new CSelectorView();
	m_busyView = new CBusyView();
}



CMainFrame::~CMainFrame()
{
	if (m_brokerView) {
		delete m_brokerView;
	}
	if (m_selectorView) {
		delete m_selectorView;
	}
	if (m_busyView) {
		delete m_busyView;
	}
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	// create a view to occupy the client area of the frame
	if (!m_brokerView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create broker view window\n");
		return -1;
	}
	if (!m_selectorView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST+1, NULL))
	{
		TRACE0("Failed to create selector view window\n");
		return -1;
	}
	if (!m_busyView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST+1, NULL))
	{
		TRACE0("Failed to create selector view window\n");
		return -1;
	}

	//if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
	//	| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
	//	!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	//{
	//	TRACE0("Failed to create toolbar\n");
	//	return -1;      // fail to create
	//}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	HICON icon=LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	SetIcon(icon, FALSE);

	m_currentView = m_brokerView;

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
// 	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
// 	EnableDocking(CBRS_ALIGN_ANY);
// 	DockControlBar(&m_wndToolBar);
	WTSRegisterSessionNotification( m_hWnd, NOTIFY_FOR_THIS_SESSION);

	 //WTSUnRegisterSessionNotification
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.style &= ~WS_SYSMENU;
	cs.style &= ~WS_SIZEBOX;
	cs.style |= WS_MAXIMIZE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.dwExStyle |= WS_EX_TOPMOST;
	cs.lpszClass = AfxRegisterWndClass(CS_NOCLOSE);
	cs.cy = ::GetSystemMetrics(SM_CYSCREEN);
	cs.cx = ::GetSystemMetrics(SM_CXSCREEN);
	cs.y = 0;
	cs.x = 0;
	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd* /*pOldWnd*/)
{
	// forward focus to the view window
	m_currentView->SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_currentView->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

LRESULT CMainFrame::OnAuthenticate(WPARAM wParam, LPARAM lParam)
{
	Broker *broker = (Broker*) lParam;
	if (broker == m_currentBroker && !m_isLogin) {
		m_isLogin = TRUE;
		CLoginDialog loginDialog;
		loginDialog.SetBroker(broker);
		loginDialog.DoModal();
		m_isLogin = FALSE;
		if (broker->hasSession() && broker->isNew()) {
			m_brokerView->AddBroker(broker);
		}
	}
	return 0;
}

LRESULT CMainFrame::OnBrokerConnect(WPARAM wParam, LPARAM lParam)
{
	m_currentBroker = (Broker *) lParam;
	return 0;
}

LRESULT CMainFrame::OnBrokerDisconnected(WPARAM wParam, LPARAM lParam)
{
	Broker* broker = (Broker*) lParam;
	if (broker == m_currentBroker) {
		RECT rect;
		GetClientRect(&rect);
		SetBusyText(_T(""));
		m_currentView->ShowWindow(SW_HIDE);
		m_currentView = m_brokerView;
		m_currentBroker = NULL;
		m_brokerView->ShowWindow(SW_SHOW);
		m_brokerView->MoveWindow(&rect);
		if (wParam > 0) {
			AfxMessageBox(_T("服务器连接出错。"));  
		}
		if (broker->isNew()) {
			delete broker;
		}
	}
	return 0;
}

LRESULT CMainFrame::OnLaunchItemUpdated(WPARAM wParam, LPARAM lParam)
{
	Broker *broker = (Broker*)lParam;
	if (broker == m_currentBroker) {
		SetBusyText(_T(""));
		RECT rect;
		GetClientRect(&rect);
		if (m_currentView != m_selectorView) {
			m_currentView->ShowWindow(SW_HIDE);
		}
		m_selectorView->SetBroker(m_currentBroker);
		m_currentView = m_selectorView;
		m_currentView->ShowWindow(SW_SHOW);
		m_currentView->MoveWindow(&rect);
		m_selectorView->Refresh();
		LaunchItem *default = m_currentBroker->GetDefaultLaunchItem();
		if (default) {
			TRACE("Trying to connect default launch item\n");
			OnLaunchItemClicked(0, (LPARAM)default);
		}
	}
	return 0;
}

LRESULT CMainFrame::OnLaunchItemClicked(WPARAM wParam, LPARAM lParam)
{
	bool ret = true;
	if (m_selectorView == m_currentView) {
		SetBusyText(_T("正在查找可用主机"));
		ret = m_selectorView->Launch((LaunchItem*) lParam);
	}
	if (m_brokerView == m_currentView) {
		Broker* data = (Broker*)lParam;
		if (data) {
			SetBusyText(_T("正在连接服务器"));
		}
		ret = m_brokerView->Launch(data);
	}
	if (!ret) {
		SetBusyText(_T(""));
		AfxMessageBox(_T("操作错误"));
	}
	return 0;
}

void CMainFrame::SetBusyText(LPCTSTR text)
{
	if ( text && _tcsclen(text) > 0 ) 
	{
		if (m_currentView != m_busyView) {
			RECT rect;
			GetClientRect(&rect);
			m_currentView->ShowWindow(SW_HIDE);
			m_busyView->SetBusy(text, m_currentView);
			m_currentView = m_busyView;
			m_busyView->ShowWindow(SW_SHOW);
			m_busyView->MoveWindow(&rect);
		} else {
			// only update the text
			m_busyView->SetBusy(text, NULL);
		}
	} else {
		if (m_currentView == m_busyView) {
			RECT rect;
			GetClientRect(&rect);
			m_currentView = m_busyView->CleanBusy();
		}
	}
}

LRESULT CMainFrame::OnLaunchItemConnect(WPARAM wParam, LPARAM lParam)
{
	SetBusyText(_T(""));
	RECT rect;
	GetClientRect(&rect);
	m_currentView->ShowWindow(SW_SHOW);
	m_currentView->MoveWindow(&rect);
	if (m_currentView == m_selectorView) {
		m_selectorView->Connect((LaunchItem*) lParam);
	}
	return 0;
}


void CMainFrame::OnDisconnect()
{
	if (m_currentBroker) {
		m_currentBroker->Disconnect();
		RECT rect;
		GetClientRect(&rect);
		SetBusyText(_T(""));
		m_currentView->ShowWindow(SW_HIDE);
		m_currentView = m_brokerView;
		m_currentBroker = NULL;
		m_brokerView->ShowWindow(SW_SHOW);
		m_brokerView->MoveWindow(&rect);
	}
}

LRESULT CMainFrame::OnLaunchItemFailed(WPARAM wParam, LPARAM lParam)
{
	SetBusyText(_T(""));
	RECT rect;
	GetClientRect(&rect);
	m_currentView->ShowWindow(SW_SHOW);
	m_currentView->MoveWindow(&rect);
	LaunchItem *launchItem = (LaunchItem*) lParam;
	if (launchItem) {
		AfxMessageBox(CA2CT(launchItem->GetStatusMessage(), CP_UTF8));  
	}
	return 0;
}


void CMainFrame::OnUpdateDisconnect(CCmdUI *pCmdUI)
{
	if (m_currentBroker) {
		pCmdUI->Enable(TRUE);
	} else {
		pCmdUI->Enable(FALSE);
	}
}

void CMainFrame::OnUpdateAppExit(CCmdUI *pCmdUI)
{
	if (gEnableAppExit) {
		pCmdUI->Enable(TRUE);
	} else {
		pCmdUI->Enable(FALSE);
	}
}


void CMainFrame::OnClose()
{
	if (m_currentBroker) {
		m_currentBroker->Disconnect();
		m_currentBroker = NULL;
	}
	CFrameWnd::OnClose();
}


LRESULT CMainFrame::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your specialized code here and/or call the base class
	if (message == WM_NCLBUTTONDBLCLK && HTCAPTION == wParam) {
		return 0;
	}

	return CFrameWnd::DefWindowProc(message, wParam, lParam);
}

//LRESULT CMainFrame::OnNcHitTest(CPoint point)
//{
//	// TODO: Add your message handler code here and/or call default
//	LRESULT result = CFrameWnd::OnNcHitTest(point);
//	TRACE("*********************OnNcHitTest %d****************\n", result);
//	if (result == HTCAPTION) {
//		result = HTCLIENT;
//	}
//	return result;
//}

void CMainFrame::OnMoving(UINT fwSide, LPRECT pRect)
{
	CFrameWnd::OnMoving(fwSide, pRect);

	// TODO: Add your message handler code here
	// Find Current location of the dialog
	CRect CurRect;
	GetWindowRect(&CurRect);

	// Set current location as the moving location
	pRect->left = CurRect.left;
	pRect->top = CurRect.top;
	pRect->right = CurRect.right;
	pRect->bottom = CurRect.bottom;
}


void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	switch (nID) {
		case SC_MINIMIZE:
		case SC_CLOSE:
			return;
		default:
			break;
	}
	CFrameWnd::OnSysCommand(nID, lParam);
}


void CMainFrame::OnSessionChange(UINT nSessionState, UINT nId)
{

	CFrameWnd::OnSessionChange(nSessionState, nId);
	// This feature requires Windows XP or greater.
	// The symbol _WIN32_WINNT must be >= 0x0501.
	// TODO: Add your message handler code here and/or call default
	DWORD pid = GetCurrentProcessId();
	DWORD sessionId = 0;
	if (!ProcessIdToSessionId(pid, &sessionId)) {
		TRACE("Cannot get session id for process %d: %d\n", pid, GetLastError());
		return;
	}
	TRACE("Receive session change message %d for session %d, our session %d\n",nSessionState, nId, sessionId);

	if (nSessionState != WTS_SESSION_LOCK || sessionId != nId || m_currentView != m_selectorView) {
		return;
	}
	m_selectorView->Lock();
}


void CMainFrame::OnDestroy()
{
	WTSUnRegisterSessionNotification(m_hWnd);
	CFrameWnd::OnDestroy();

	// TODO: Add your message handler code here
}
