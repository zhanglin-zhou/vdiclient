﻿// RemoteFrame.cpp : implementation file
//

#include "stdafx.h"
#include "vdiClient.h"
#include "RDPRemoteFrame.h"
#include "CMsRdpClientAdvancedSettings7.h"


// CRemoteFrame

//IMPLEMENT_DYNCREATE(CRemoteFrame, CFrameWnd)

CRDPRemoteFrame::CRDPRemoteFrame()
	:m_launchItem(NULL)
{

}

CRDPRemoteFrame::~CRDPRemoteFrame()
{
	TRACE("CRDPRemoteFrame::~CRDPRemoteFrame\n");
}


BEGIN_MESSAGE_MAP(CRDPRemoteFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_DISCONNECT, &CRDPRemoteFrame::OnDisconnect)
	ON_COMMAND(ID_APP_EXIT, &CRDPRemoteFrame::OnAppExit)
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CRDPRemoteFrame, CFrameWnd)
	ON_EVENT(CRDPRemoteFrame, AFX_IDW_PANE_FIRST, 0x2, OnConnected, VTS_NONE)
	ON_EVENT(CRDPRemoteFrame, AFX_IDW_PANE_FIRST, 0x4, OnDisconnected, VTS_I4)
	ON_EVENT(CRDPRemoteFrame, AFX_IDW_PANE_FIRST, 0x5, OnEnterFullScreenMode, VTS_NONE)
	ON_EVENT(CRDPRemoteFrame, AFX_IDW_PANE_FIRST, 0x6, OnLeaveFullScreenMode, VTS_NONE)
	ON_EVENT(CRDPRemoteFrame, AFX_IDW_PANE_FIRST, 0xc, OnRemoteDesktopSizeChange, VTS_I4 VTS_I4)
END_EVENTSINK_MAP()

// CRemoteFrame message handlers

int CRDPRemoteFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	RECT rect = {0};
	m_rdpCtrl.CreateControl(L"MsTscAx.MsTscAx", NULL, WS_CHILD|WS_VISIBLE|WS_GROUP, rect, this, AFX_IDW_PANE_FIRST);
	return 0;
}

void CRDPRemoteFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);
	// TODO: Add your message handler code here
	TRACE("OnSize: %d %d\n", cx, cy);
	m_rdpCtrl.MoveWindow(0, 0, cx, cy);
	//m_rdpClient.put_DesktopWidth(cx);
	//m_rdpClient.put_DesktopHeight(cy);
}

void CRDPRemoteFrame::Connect(const char* host, const char* domain,  const char* username, const char* password)
{
	IUnknown * pUnk = m_rdpCtrl.GetControlUnknown();
	if(CComQIPtr<IDispatch> pDisp = pUnk){
		m_rdpClient.AttachDispatch(pDisp);
		CMsRdpClientAdvancedSettings7 rdp_settings(m_rdpClient.get_AdvancedSettings());
		rdp_settings.put_RedirectPrinters(TRUE);
		rdp_settings.put_singleConnectionTimeout(20);
		rdp_settings.put_Compress(1);
		rdp_settings.put_SmartSizing(TRUE);
		rdp_settings.put_EnableWindowsKey(TRUE);
		rdp_settings.put_GrabFocusOnConnect(TRUE);
		rdp_settings.put_DisplayConnectionBar(FALSE);
		rdp_settings.put_ConnectionBarShowMinimizeButton(FALSE);
		rdp_settings.put_ConnectionBarShowRestoreButton(FALSE);
		m_rdpClient.put_Server(CA2CT(host));
		m_rdpClient.put_FullScreen(TRUE);
		m_rdpClient.put_ColorDepth(32);
		m_rdpClient.put_ConnectingText(_T("正在连接云主机，请稍等。"));
		MONITORINFO monitorInfo = {0};
		monitorInfo.cbSize = sizeof(MONITORINFO);
		HMONITOR monitor = NULL;
		monitor = MonitorFromWindow(this->m_hWnd, MONITOR_DEFAULTTOPRIMARY);
		if (!GetMonitorInfo(monitor, &monitorInfo)) {
			TRACE("GetMonitorInfo failed: %d\n",  GetLastError());
		}
		m_rdpClient.put_DesktopHeight(monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top);
		m_rdpClient.put_DesktopWidth(monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left);

		m_rdpClient.put_Domain(CA2CT(domain));
		m_rdpClient.put_UserName(CA2CT(username));
		rdp_settings.put_ClearTextPassword(CA2CT(password));

// 		m_rdpClient.put_Domain(_T("crt"));
// 		m_rdpClient.put_UserName(_T("administrator"));
// 		rdp_settings.put_ClearTextPassword(_T("ca$hc0w"));

		m_rdpClient.Connect();
		short result = m_rdpClient.get_Connected();
		AfxGetMainWnd()->ShowWindow(SW_HIDE);
	}
}

void CRDPRemoteFrame::OnDestroy()
{
	CFrameWnd::OnDestroy();
	// TODO: Add your message handler code here
	m_rdpClient.DetachDispatch();
	m_rdpCtrl.DestroyWindow();
	AfxGetMainWnd()->ShowWindow(SW_SHOW);
	AfxGetMainWnd()->SetActiveWindow();
}

void CRDPRemoteFrame::OnConnected()
{
	m_launchItem->SetStatus(CONNECTED);
}

void CRDPRemoteFrame::OnDisconnected(long reason)
{
	TRACE("OnDisconnected: %d\n", reason);
	switch (reason)  
	{  
	case 0x3:  
		AfxMessageBox(_T("另一用户已连接到此主机，因此您的连接已断开。"));  
		break;  
	default:  
		break;  
	} 
	m_launchItem->Disconect();
}

void CRDPRemoteFrame::OnRemoteDesktopSizeChange(long width, long height)
{
	TRACE("OnRemoteDesktopSizeChange: %d %d, focus: 0x%x\n", width, height, ::GetFocus());
}


void CRDPRemoteFrame::OnClose()
{
	if (m_rdpClient.get_Connected()) {
		m_rdpClient.Disconnect();
	}
	m_launchItem->setRemoteWindow(NULL);
	m_launchItem->SetStatus(FREE);
	m_launchItem = NULL;
	CFrameWnd::OnClose();
}

void CRDPRemoteFrame::OnEnterFullScreenMode()
{
	TRACE("OnEnterFullScreenMode\n");
	ShowWindow(SW_HIDE);
}

void CRDPRemoteFrame::OnLeaveFullScreenMode()
{
	TRACE("OnLeaveFullScreenMode\n");
	ShowWindow(SW_SHOW);
}

void CRDPRemoteFrame::OnSetFocus(CWnd* pOldWnd)
{
	// TODO: Add your message handler code here
	HWND previous = ::SetFocus(m_rdpCtrl.m_hWnd);
	if (previous == NULL) {
		TRACE("CRDPRemoteFrame::OnSetFocus error:%d\n", ::GetLastError());
	} else {
		TRACE("CRDPRemoteFrame::OnSetFocus previous 0x%x, current: 0x%x\n", previous, ::GetFocus());
	}
}


void CRDPRemoteFrame::OnDisconnect()
{
	m_launchItem->Disconect();
	//m_launchItem = NULL;
}

void CRDPRemoteFrame::OnAppExit()
{
	m_launchItem->Disconect();
	//m_launchItem = NULL;
}


BOOL CRDPRemoteFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Add your specialized code here and/or call the base class
	cs.style &= ~WS_SYSMENU;
	cs.style &= ~WS_SIZEBOX;
	cs.style |= WS_MAXIMIZE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.dwExStyle |= WS_EX_TOPMOST;
	cs.lpszClass = AfxRegisterWndClass(CS_NOCLOSE);
	cs.cy = ::GetSystemMetrics(SM_CYSCREEN);
	cs.cx = ::GetSystemMetrics(SM_CXSCREEN);
	cs.y = 0;
	cs.x = 0;
	return CFrameWnd::PreCreateWindow(cs);
}
