﻿// SelectorView.cpp : implementation file
//

#include "stdafx.h"
#include "Message.h"
#include "vdiClient.h"
#include "SelectorView.h"
#include "RDPRemoteFrame.h"
#include "LaunchItemView.h"
#include "LoginDialog.h"
#include <afxtoolbarimages.h>
#include <algorithm> 


// CSelectorView

IMPLEMENT_DYNAMIC(CSelectorView, CWnd)

CSelectorView::CSelectorView()
: m_maxColumn(3),
  m_column(0),
  m_row(0),
  m_locked(FALSE),
  m_broker(NULL)
{

}

CSelectorView::~CSelectorView()
{
}


BEGIN_MESSAGE_MAP(CSelectorView, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()



// CSelectorView message handlers




void CSelectorView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CWnd::OnPaint() for painting messages
   if (m_broker->GetLaunchItemCount() == 0) {
      CRect rect;
      GetClientRect(rect);
      dc.DrawText(_T("你没有分配任何桌面，请联系系统管理员。"), rect, DT_SINGLELINE| DT_VCENTER | DT_CENTER | DT_NOPREFIX);
   }
}

BOOL CSelectorView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

int CSelectorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	LoadImage();
	RECT rectClient;
	GetWindowRect(&rectClient);
	m_grid.Create(rectClient, this, IDC_SELECTOR_GRID);
	m_grid.SetDefaultCellType(RUNTIME_CLASS(LaunchItemView));
	m_grid.SetImageList(&m_imageList);
	m_grid.SetEditable(FALSE);
	m_grid.SetGridLines(GVL_NONE);
	m_grid.SetDefCellHeight(LaunchItemView::GetHeight());
	m_grid.SetDefCellWidth(LaunchItemView::GetWidth());
	m_grid.SetGridBkColor(RGB(255,255,255));
	m_grid.SetColumnCount(0);
	m_grid.SetRowCount(0);
	return 0;
}

void CSelectorView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_grid.MoveWindow(0,0, cx, cy);
	if (cx > 0) {
		m_maxColumn = cx / LaunchItemView::GetWidth();
	}
	Refresh();
}

void CSelectorView::Refresh()
{
	if (!m_broker) {
		return;
	}
	int cnt = m_broker->GetLaunchItemCount();
	if (cnt) {
		m_grid.SetRedraw(FALSE);
		if (cnt < m_maxColumn) {
			m_column = cnt;
			m_row = 1;
		} else {
			m_column = m_maxColumn;
			m_row = cnt/m_column + 1;
		}
		m_grid.SetColumnCount(m_column);
		m_grid.SetRowCount(m_row);

		int row = 0;
		int column = 0;
		CSingleLock lock(&(m_broker->launchItemsLock), TRUE);
		for (map<string, LaunchItem*>::iterator it = m_broker->launchItems.begin(); it != m_broker->launchItems.end(); it++) {
			if (column == m_column) {
				row++;
				column = 0;
			}
			LaunchItem *launchItem = it->second;
			m_grid.SetItemText(row, column, CA2CT(launchItem->GetName(), CP_UTF8));
			m_grid.SetItemImage(row, column, 0);
			m_grid.SetItemData(row, column, (LPARAM)launchItem);
			column++;
		}
		m_grid.ShowWindow(SW_SHOW);
		m_grid.SetRedraw(TRUE);
		m_grid.Invalidate();
		m_grid.UpdateWindow();
	} else {
		m_grid.ShowWindow(SW_HIDE);
	}
}

bool CSelectorView::Launch(LaunchItem *data)
{
	LaunchItem* launchItem = dynamic_cast<LaunchItem*>(data);
	if (launchItem) {
		launchItem->Launch();
		return true;
	} else {
		TRACE("It's not a LaunchItem pointer: %d\n", data);
		return false;
	}
}

void CSelectorView::LoadImage()
{
	BOOL ret = m_imageList.Create(128, 128, ILC_COLOR32 | ILC_MASK, 0, 4);
	CPngImage desktopImage;
	CBitmap desktopBmp;
	desktopImage.Load(IDB_PNG4);
	desktopBmp.Attach(desktopImage.Detach());
	m_imageList.Add(&desktopBmp, RGB(255,0,255));
}

void CSelectorView::Connect(LaunchItem *launchItem)
{
	int status = launchItem->GetStatus();
	if (status != CONNECTING || status != CONNECTED ) {
		CRDPRemoteFrame* pFrame = new CRDPRemoteFrame;
		if (!pFrame)
			return;
		pFrame->LoadFrame(IDR_MAINFRAME,
						  WS_MAXIMIZE|WS_CLIPCHILDREN,
						  this,
						 NULL);
		// The one and only window has been initialized, so show and update it
		pFrame->ShowWindow(SW_SHOW);
		pFrame->UpdateWindow();
		pFrame->Connect(launchItem->GetSelectedAddress(), m_broker->GetLoginDomain(), m_broker->GetUserName(), m_broker->GetPassword());
		launchItem->setRemoteWindow(pFrame);
		pFrame->setLaunchItem(launchItem);
		launchItem->SetStatus(CONNECTING);
	}
	return;	
}

void CSelectorView::Lock()
{
	if (m_locked || !m_broker) {
		return;
	}
	m_locked = TRUE;
	CLoginDialog loginDialog;
	loginDialog.SetBroker(m_broker);
	AfxGetMainWnd()->ShowWindow(SW_SHOW);
	AfxGetMainWnd()->SetActiveWindow();
	switch (loginDialog.DoLock()) {
	case IDOK:
		if (m_broker->hasConnectedLaunchItem()) {
			AfxGetMainWnd()->ShowWindow(SW_HIDE);
		}
		break;
	case IDCANCEL:
		break;
	default:
		break;
	}
	m_locked = FALSE;
}