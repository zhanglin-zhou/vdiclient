// BrokerView.h : interface of the CBrokerView class
//


#pragma once
#include "GridCtrl/GridCtrl.h"
#include <vector>
#include "Broker.h"

using namespace std;
// CBrokerView window

class CBrokerView : public CWnd
{
// Construction
public:
	CBrokerView();

// Attributes
public:
	void AddBroker(Broker* broker);

// Operations
public:

// Overrides
protected:
	CWnd rdpWnd;
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CBrokerView();

private:
	void LoadBrokerHistory();
	void LoadImage();
	void LoadDefaultBroker();
	void Refresh();
	vector<Broker*> m_brokers;
	Broker *m_defaultBroker;
	CImageList m_imageList;
	int m_maxColumn;
	int m_column;
	int m_row;
	// Generated message map functions
protected:
	CGridCtrl m_grid;
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	bool Launch(Broker* data);
	void AddServer(CString &server);
};

