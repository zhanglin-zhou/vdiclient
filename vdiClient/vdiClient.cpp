// vdiClient.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "vdiClient.h"
#include "MainFrm.h"
#include <curl/curl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BOOL gEnableAppExit = FALSE;

// CvdiClientApp

BEGIN_MESSAGE_MAP(CvdiClientApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CvdiClientApp::OnAppAbout)
END_MESSAGE_MAP()


// CvdiClientApp construction

CvdiClientApp::CvdiClientApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

CvdiClientApp::~CvdiClientApp()
{
	curl_global_cleanup();
}

// The one and only CvdiClientApp object

CvdiClientApp theApp;


// CvdiClientApp initialization

BOOL CvdiClientApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();

	curl_global_init(CURL_GLOBAL_NOTHING);

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	CMainFrame* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;
	// create and load the frame with its resources
	pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL);
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	return TRUE;
}


// CvdiClientApp message handlers




// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

// App command to run the dialog
void CvdiClientApp::OnAppAbout()
{
 	CAboutDlg aboutDlg;
 	aboutDlg.DoModal();

}


// CvdiClientApp message handlers



void CAboutDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	static int cnt = 0;
	cnt++;
	if (cnt > 5) {
		gEnableAppExit = TRUE;
	}
	CDialog::OnLButtonDblClk(nFlags, point);
}
