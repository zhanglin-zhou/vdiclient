#pragma once
#include "CMsRdpClient7.h"
#include "LaunchItem.h"
#include <string>
using namespace std;
// CRemoteFrame frame

class CRDPRemoteFrame : public CFrameWnd
{
public:
	CRDPRemoteFrame();           // protected constructor used by dynamic creation
	virtual ~CRDPRemoteFrame();

public:
	void setAddress(const char* address) { m_address = address;}
	void setLaunchItem(LaunchItem *launchItem) { m_launchItem = launchItem;}
protected:
	DECLARE_MESSAGE_MAP()
	DECLARE_EVENTSINK_MAP()
private:
	CMsRdpClient7 m_rdpClient;
	CWnd m_rdpCtrl;
	string m_address;
	LaunchItem *m_launchItem;

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	void Connect(const char* host, const char* domain,  const char* username, const char* password);
	afx_msg void OnDestroy();
	afx_msg void OnDisconnected(long reason);
	afx_msg void OnConnected();
	afx_msg void OnRemoteDesktopSizeChange(long width, long height);
	afx_msg void OnEnterFullScreenMode();
	afx_msg void OnLeaveFullScreenMode();
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnDisconnect();
	afx_msg void OnAppExit();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
};


