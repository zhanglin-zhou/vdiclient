#pragma once


// CBusyView

class CBusyView : public CWnd
{
	DECLARE_DYNAMIC(CBusyView)

public:
	CBusyView();
	virtual ~CBusyView();
	void SetBusy(LPCTSTR text, CWnd *previousView);
	CWnd* CleanBusy();
private:
	CString m_busyText;
	CWnd *m_previousView;
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

};


