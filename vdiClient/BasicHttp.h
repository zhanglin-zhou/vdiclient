#pragma once
#include <list>
#include <string>
using namespace std;
class BasicHttpRequest
{
public:
	BasicHttpRequest(void);
	~BasicHttpRequest(void);

public:
	void setUrl(const char* url);
private:
	int perform(int http_method);
private:
	string m_userAgent;
	string m_url;
	string m_body;
	string m_response;
	string m_cookie;
	list<string> m_headers;
	long m_responseCode;

public:
	void addHeader(const char* header);
	void setBody(const char* body);
	int doGet(void);
	int doPost(void);
	long getResponseCode(void);
	const char* getResponse();
	const char* getCookie() { return m_cookie.c_str();}
	void setCookie(const char* cookie);
private:
	void setResponse(const char* response);
private:
	static size_t header_callback(char *buffer,   size_t size,   size_t nitems,   void *userdata);
	static size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata); 
};

