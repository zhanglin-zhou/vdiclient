﻿// BrokerView.cpp : implementation of the CBrokerView class
//

#include "stdafx.h"
#include "vdiClient.h"
#include "BrokerView.h"
#include "Broker.h"
#include <afxtoolbarimages.h>
#include "LaunchItemView.h"
#include "LoginDialog.h"
#include "AddServerDialog.h"
#include "Message.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CBrokerView

CBrokerView::CBrokerView()
	:m_maxColumn(3),
	 m_defaultBroker(NULL)
{
}

CBrokerView::~CBrokerView()
{
	for (std::vector<Broker*>::iterator it = m_brokers.begin(); it != m_brokers.end(); ++it) {
		delete *it;
	}
	if (m_defaultBroker) {
		delete m_defaultBroker;
		m_defaultBroker = NULL;
	}
}


BEGIN_MESSAGE_MAP(CBrokerView, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CBrokerView message handlers

BOOL CBrokerView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);
	return TRUE;
}

void CBrokerView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	// TODO: Add your message handler code here
	
	// Do not call CWnd::OnPaint() for painting messages
}


int CBrokerView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	LoadBrokerHistory();
	LoadDefaultBroker();
	LoadImage();
	RECT rectClient;
	GetWindowRect(&rectClient);
	m_grid.Create(rectClient, this, IDC_BROKER_GRID);
 	m_grid.SetDefCellHeight(LaunchItemView::GetHeight());
 	m_grid.SetDefCellWidth(LaunchItemView::GetWidth());
	m_grid.SetDefaultCellType(RUNTIME_CLASS(LaunchItemView));
	m_grid.SetImageList(&m_imageList);
	m_grid.SetEditable(FALSE);
	m_grid.SetGridLines(GVL_NONE);
	m_grid.SetGridBkColor(RGB(255,255,255));
	m_grid.SetColumnCount(0);
	m_grid.SetRowCount(0);

	Broker *default = m_defaultBroker;
	if (!default && m_brokers.size() == 1) {
		default = m_brokers.front();
	}
	if (default) {
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITEMCLICKED, 0, (LPARAM) default);
	}

	return 0;
}

void CBrokerView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: your message handler code here
	m_grid.MoveWindow(0, 0, cx, cy);
	if (cx > 0) {
		m_maxColumn = cx / LaunchItemView::GetWidth();
	}
	Refresh();
}

void CBrokerView::AddBroker(Broker* broker)
{
	CRegKey key;
	CRegKey subKey;
	TCHAR buffer[1024];
	DWORD length = sizeof(buffer)/sizeof(TCHAR);
	key.Create(HKEY_CURRENT_USER, _T("Software\\Wisvdi\\server"));
	subKey.Create(key, CA2CT(broker->GetName(), CP_UTF8));
	subKey.SetStringValue(_T("url"), CA2CT(broker->GetUrl(), CP_UTF8));
	subKey.Close();
	key.Close();
	m_brokers.push_back(broker);
}

void CBrokerView::Refresh()
{
 	m_grid.SetRedraw(FALSE);
	int cnt = m_brokers.size()+1;
	if (cnt <= m_maxColumn) {
		m_column = cnt;
		m_row = 1;
	} else {
		m_column = m_maxColumn;
		m_row = cnt/m_column + 1;
	}
	m_grid.SetColumnCount(m_column);
	m_grid.SetRowCount(m_row);
	int row = 0;
	int column = 0;
	for (int i = 0; i < cnt; i++) {
		if (column == m_column) {
			row++;
			column = 0;
		}
		if (i<cnt-1) {
			m_grid.SetItemText(row, column, CA2CT(m_brokers[i]->GetName(), CP_UTF8));
			m_grid.SetItemImage(row, column, 1);
			m_grid.SetItemData(row, column, (LPARAM)m_brokers[i]);
		} else {
			m_grid.SetItemText(row, column, _T("添加服务器"));
			m_grid.SetItemImage(row, column, 0);
			m_grid.SetItemData(row, column, (LPARAM)NULL);
		}
		column++;
	}
 	m_grid.SetRedraw(TRUE);
	m_grid.Invalidate();
	m_grid.UpdateWindow();
}

void CBrokerView::LoadBrokerHistory()
{
	CRegKey key;
	TCHAR buffer[128];
	DWORD length = sizeof(buffer)/sizeof(TCHAR);
	key.Create(HKEY_CURRENT_USER, _T("Software\\Wisvdi\\server"));
	int index = 0;
	while (key.EnumKey(index, buffer, &length, NULL) == ERROR_SUCCESS) {
		TCHAR buffer2[1024];
		DWORD length2 = sizeof(buffer2)/sizeof(TCHAR);
		CRegKey serverKey;
		serverKey.Create(key, buffer);
		serverKey.QueryStringValue(_T("url"), buffer2, &length2);
		Broker *broker = new Broker(CT2CA(buffer2));
		broker->SetName(CT2CA(buffer, CP_UTF8));
		m_brokers.push_back(broker);
		serverKey.Close();
	}
	key.Close();
}

void CBrokerView::LoadDefaultBroker()
{
	CRegKey key;
	TCHAR buffer[128] = {0};
	DWORD length = sizeof(buffer)/sizeof(TCHAR);
	key.Create(HKEY_CURRENT_USER, _T("Software\\Wisvdi\\default"));
	if (key.QueryStringValue(_T("url"), buffer, &length) == ERROR_SUCCESS) {
		m_defaultBroker = new Broker(CT2CA(buffer));
		if (key.QueryStringValue(_T("autoconnect"), buffer, &length) == ERROR_SUCCESS) {
			m_defaultBroker->SetAutoConnect(CT2CA(buffer));
		}
	}
	key.Close();
}

void CBrokerView::LoadImage()
{
	BOOL ret = m_imageList.Create(128, 128, ILC_COLOR32 | ILC_MASK, 0, 4);
	CPngImage addImage, serverImage;
	CBitmap addBmp, serverBmp;
	addImage.Load(IDB_PNG3);
	addBmp.Attach(addImage.Detach());
	serverImage.Load(IDB_PNG5);
	serverBmp.Attach(serverImage.Detach());
	m_imageList.Add(&addBmp, RGB(255,0,255));
	m_imageList.Add(&serverBmp, RGB(255,0,255));
}

bool CBrokerView::Launch(Broker* data)
{
	if (!data) {
		//add broker
		CAddServerDialog addServerDialog(this);
		addServerDialog.DoModal();
		return true;
	} else {
		Broker* broker = dynamic_cast<Broker*>(data);
		 if (broker) {
			 broker->Launch();
			 return true;
		 } else {
			 TRACE("It's not a broker pointer: %d\n", data);
			 return false;
		 }
	}
}

void CBrokerView::AddServer(CString &server)
{
	Broker *broker = new Broker(CT2CA(server), TRUE);
	broker->Launch();
}


