// vdiClient.h : main header file for the vdiClient application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CvdiClientApp:
// See vdiClient.cpp for the implementation of this class
//

class CvdiClientApp : public CWinApp
{
public:
	CvdiClientApp();
	~CvdiClientApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CvdiClientApp theApp;