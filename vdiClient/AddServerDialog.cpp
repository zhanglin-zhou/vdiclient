// AddServerDialog.cpp : implementation file
//

#include "stdafx.h"
#include "vdiClient.h"
#include "AddServerDialog.h"
#include "afxdialogex.h"
#include "BrokerView.h"
#include "Message.h"


// CAddServerDialog dialog

IMPLEMENT_DYNAMIC(CAddServerDialog, CDialogEx)

CAddServerDialog::CAddServerDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAddServerDialog::IDD, pParent)
{

}

CAddServerDialog::~CAddServerDialog()
{
}

void CAddServerDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAddServerDialog, CDialogEx)
END_MESSAGE_MAP()

void CAddServerDialog::OnOK()
{
	CString server;
	m_addServerCtrl->GetWindowTextW(server);
	CBrokerView *brokerView = (CBrokerView*) m_pParentWnd;
	Broker *broker = new Broker(CT2CA(server), TRUE);
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITEMCLICKED, 0, (LPARAM)broker);
	return CDialogEx::OnOK();
}

BOOL CAddServerDialog::OnInitDialog()
{
	m_addServerCtrl = (CEdit *) GetDlgItem(IDC_EDIT1);
	return TRUE;
}


// CAddServerDialog message handlers
