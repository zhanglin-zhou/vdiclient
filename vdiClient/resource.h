//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by vdiClient.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_vdiClientTYPE               129
#define ID_DISCONNECT					130
#define IDB_PNG1                        133
#define IDB_PNG2                        134
#define IDB_PNG3                        135
#define IDB_PNG4                        136
#define IDB_PNG5                        137
#define IDC_BROKER_GRID                 139
#define IDC_SELECTOR_GRID               140
#define IDD_DLG_LOGIN                   141
#define IDB_PNG6                        142
#define IDI_ICON1                       144
#define IDI_ICON2                       145
#define IDB_PNG7                        146
#define IDD_DLG_ADDSERVER               147
#define IDC_DLG_IMAGE                   1008
#define IDC_CUSTOM1                     1009
#define IDC_EDIT1                       1010
#define IDC_EDIT2                       1011
#define IDC_COMBOBOXEX1                 1012
#define IDC_COMBO1                      1013
#define IDC_CUSTOM2                     1014
#define IDC_LOGIN_MSG                   1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
