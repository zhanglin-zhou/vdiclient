#pragma once


// CAddServerDialog dialog

class CAddServerDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CAddServerDialog)

public:
	CAddServerDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddServerDialog();

// Dialog Data
	enum { IDD = IDD_DLG_ADDSERVER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	virtual void OnOK();

	virtual BOOL OnInitDialog();
private:
	CEdit *m_addServerCtrl;

};
