#include "stdafx.h"
#include "LaunchItemView.h"
#include "GridCtrl/GridCtrl.h"
#include "Message.h"

IMPLEMENT_DYNCREATE(LaunchItemView, CGridCell)

LaunchItemView::LaunchItemView(void)
{
}


LaunchItemView::~LaunchItemView(void)
{
}

BOOL LaunchItemView::GetTextRect(LPRECT pRect) 
{
	if (GetImage() >= 0)
	{
		IMAGEINFO Info;

		CGridCtrl* pGrid = GetGrid();
		CImageList* pImageList = pGrid->GetImageList();

		if (pImageList && pImageList->GetImageInfo( GetImage(), &Info))
		{
			int nImageWidth = Info.rcImage.right-Info.rcImage.left+1;
			int nImageHeight = Info.rcImage.bottom-Info.rcImage.top+1;
			//pRect->left += nImageWidth + GetMargin();
			pRect->top += nImageHeight + GetMargin();
		}
	}

	return TRUE;
}

DWORD LaunchItemView::GetFormat() const
{
	DWORD format = CGridCell::GetFormat();
	return format | DT_BOTTOM | DT_CENTER;
}

BOOL LaunchItemView::Draw(CDC* pDC, int nRow, int nCol, CRect rect, BOOL bEraseBkgnd /*= TRUE*/)
{
	IMAGEINFO Info;
	CGridCtrl* pGrid = GetGrid();
	CImageList* pImageList = pGrid->GetImageList();

	if (pImageList && pImageList->GetImageInfo( GetImage(), &Info))
	{
		int nImageWidth = Info.rcImage.right-Info.rcImage.left+1;
		rect.left += (rect.right-rect.left-nImageWidth)/2;
	}
	return CGridCell::Draw(pDC, nRow, nCol, rect, bEraseBkgnd);
}

void LaunchItemView::OnDblClick(CPoint PointCellRelative)
{
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITEMCLICKED, 0, GetData());
}
