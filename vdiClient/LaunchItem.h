#pragma once
#include <string>
#include <vector>
#include <list>
#include <afxmt.h>
using namespace std;


#define FREE 0
#define CONNECTING 1
#define CONNECTED 2

class Broker;

class LaunchItem
{
public:
	LaunchItem(Broker *broker);
	~LaunchItem(void);
	int Launch(void);
	void Disconect();
	CCriticalSection AddressesLock;
public:
	const char* GetId() const { return m_id.c_str(); }
	void SetId(const char* val) { m_id = val; }
	const char* GetName() const { return m_name.c_str(); }
	void SetName(const char* val) { m_name = val; }
	void SetStatus(int status) { m_status = status;}
	int GetStatus() { return m_status;}
	const char* GetStatusMessage() { return m_statusMessage.c_str();};
	int GetAddressCount() { return m_addresses.size();}
	void ClearAddress() { m_addresses.clear();}
	const char* GetAddress(int index) { return m_addresses[index].c_str();}
	const char* GetSelectedAddress();
	void addAddress(const char* address) { m_addresses.push_back(address);}
	void addValidAddress(const char* address);
	void addFailedAddress(const char* address);
	bool hasRemoteWindow() { return m_remoteWindow != NULL;}
	void setRemoteWindow(CWnd* pWnd) { m_remoteWindow = pWnd;}
	bool setSelectedAddress(const char* address);
private:
	CWnd *m_remoteWindow;
	Broker *m_broker;
private:
	static UINT worker(LPVOID data);
	static VOID CALLBACK TimerProc(HWND hwnd,UINT uMsg, UINT_PTR idEvent,DWORD dwTime);
	int m_pendingAddressIndex;
private:
	string m_id;
	string m_name;
	string m_statusMessage;
	int m_status;
	vector<string> m_addresses;
	string m_selectedAddress;
	list<string> m_validAddresses;
	list<string> m_failedAddresses;
	list<void*> m_workers;
};

