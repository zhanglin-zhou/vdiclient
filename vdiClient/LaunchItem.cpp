﻿#include "stdafx.h"
#include <WinCred.h>
#include <WtsApi32.h>
#include <WinNetWk.h>
#include "Broker.h"
#include "LaunchItem.h"
#include "Message.h"

LaunchItem::LaunchItem(Broker* broker)
	:m_remoteWindow(NULL),
	 m_broker(broker),
	 m_status(FREE)
{
}


LaunchItem::~LaunchItem(void)
{
	TRACE("LaunchItem::~LaunchItem Enter\n");
	Disconect();
	TRACE("LaunchItem::~LaunchItem Exit\n");
}

int LaunchItem::Launch(void)
{
// 	m_workerEvent = new CEvent(FALSE, FALSE);
// 	m_worker = ::AfxBeginThread(worker, this);
// 	m_worker->m_bAutoDelete = FALSE;
	int cnt = GetAddressCount();
	if (cnt <= 0) {
		m_statusMessage = CT2CA(_T("未提供任何可用主机，请联系系统管理员。"), CP_UTF8);
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITMEFAILED, 0, (LPARAM)this);
		return 0;
	}
	TRACE("Got address count: %d\n", cnt);
	CSingleLock lock(&AddressesLock, TRUE);
	m_selectedAddress.clear();
	m_validAddresses.clear();
	m_failedAddresses.clear();
	m_statusMessage.clear();
	m_pendingAddressIndex = -1;
	for (int i = 0; i <= cnt; i++) {
		CWinThread *workerThread = ::AfxBeginThread(worker, this);
		m_workers.push_back(workerThread);
		workerThread->m_bAutoDelete = TRUE;
	}
	return 0;
}


UINT LaunchItem::worker(LPVOID data)
{
	LaunchItem *launchItem = (LaunchItem*) data;
	AfxSocketInit();
	bool isTimerThread = FALSE;
	const char* address = NULL;
	{
		CSingleLock lock(&(launchItem->AddressesLock), TRUE);
		if (launchItem->m_pendingAddressIndex < 0) {
			isTimerThread = TRUE;
		} else {
			address = launchItem->GetAddress(launchItem->m_pendingAddressIndex);
		}
		launchItem->m_pendingAddressIndex++;
	}
	if (isTimerThread) {
		HANDLE hTimer = NULL;
		LARGE_INTEGER liDueTime;
		liDueTime.QuadPart = -30000000LL;

		// Create an unnamed waitable timer.
		hTimer = CreateWaitableTimer(NULL, TRUE, NULL);
		if (NULL == hTimer)
		{
			TRACE("CreateWaitableTimer failed (%d)\n", GetLastError());
			return 1;
		}
		TRACE("Waiting for 3 seconds...\n");

		// Set a timer to wait for 10 seconds.
		if (!SetWaitableTimer(hTimer, &liDueTime, 0, NULL, NULL, 0))
		{
			TRACE("SetWaitableTimer failed (%d)\n", GetLastError());
			return 2;
		}

		// Wait for the timer.
		if (WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0) {
			TRACE("WaitForSingleObject failed (%d)\n", GetLastError());
		} else {
			CSingleLock lock(&(launchItem->AddressesLock), TRUE);
			if (launchItem->m_selectedAddress.empty() && launchItem->GetSelectedAddress() != NULL) {
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITMECONNECT, 0, (LPARAM)launchItem);
			}
		}
		return 0;
	}
	CSocket socket;
	socket.Create();
	if (socket.Connect(CA2CT(address), 3389)) {
		TRACE("Found available address %s\n", address);
		DWORD ret = NO_ERROR;
		DWORD activeSessionCount = 0;
		BOOL hasPreviousSession = FALSE;
		if (strcmp(launchItem->m_broker->GetLoginDomain(), "default") != 0) {
			PCREDENTIAL_TARGET_INFORMATION pcti = NULL;
			CString user(CA2CT(launchItem->m_broker->GetLoginDomain(), CP_UTF8));
			user.Append(_T("\\"));
			user.Append(CA2CT(launchItem->m_broker->GetUserName(), CP_UTF8));
			CString password(CA2CT(launchItem->m_broker->GetPassword(), CP_UTF8));
			CString target(CA2CT(address, CP_UTF8));

			CREDENTIAL_TARGET_INFORMATION cti = {0};

			cti.TargetName = target.GetBuffer();
			cti.Flags = CRED_TI_SERVER_FORMAT_UNKNOWN;
			DWORD credTypes[2] = { CRED_TYPE_GENERIC, CRED_TYPE_DOMAIN_PASSWORD};
			cti.CredTypeCount = 2;
			cti.CredTypes = &credTypes[0];
			cti.PackageName = _T("NTLM");
			pcti = &cti;

			{
				CREDENTIAL cred;
				ZeroMemory( &cred, sizeof(CREDENTIAL) );
			
				cred.Persist = CRED_PERSIST_SESSION;
				cred.Flags = 0;
				cred.Type = CRED_TYPE_DOMAIN_PASSWORD;
				cred.TargetName = pcti->TargetName;
				cred.UserName = user.GetBuffer();
				cred.CredentialBlob = (BYTE *)password.GetBuffer();
				cred.CredentialBlobSize = (password.GetLength()) * sizeof(TCHAR);
				if (!CredWriteDomainCredentials(pcti, &cred, 0)) {
					ret = GetLastError();
					TRACE("Cannot write domain credentials for target %s : %d\n", pcti->TargetName, ret);
				}
				//CredFree((PVOID)pcti);
			}
			HANDLE hServer = WTSOpenServer(CA2CT(address));
			PWTS_SESSION_INFO pSessionInfo = NULL;
			DWORD sessionCount = 0;
			if (hServer) {
				if (WTSEnumerateSessions(hServer, 0, 1, &pSessionInfo, &sessionCount)) {
					for (int i = 0; i < sessionCount; i++) {
						LPTSTR username = NULL;
						DWORD bufferLength;
						if (WTSQuerySessionInformation(hServer, pSessionInfo[i].SessionId, WTSUserName, &username, &bufferLength)) {
							if (username != NULL && username[0] != 0) {
								if (CString(CA2CT(launchItem->m_broker->GetUserName(), CP_UTF8)).Compare(username) == 0) {
									if (pSessionInfo[i].State == WTSActive || pSessionInfo[i].State == WTSDisconnected) {
										TRACE("Found previous session in server %s, username: %s, activeSessionCount: %d\n", address, username, activeSessionCount);
										if (activeSessionCount == 0 && launchItem->setSelectedAddress(address)) {
											hasPreviousSession = TRUE;
										} else {
											if (WTSLogoffSession(hServer, pSessionInfo[i].SessionId, FALSE)) {
												TRACE("Logoff previous session in server %s, username: %s successfully", address, username);
											} else {
												TRACE("Logoff previous session in server %s, username: %s failed: %d", address, username, GetLastError());
											}
										}
									}
								} else {
									if (pSessionInfo[i].State == WTSActive) {
										activeSessionCount++;
										TRACE("Found active session in server %s, username: %s\n", address, username);
									}
								}
							}
							WTSFreeMemory(username);
						} else {
							TRACE("WTSQuerySessionInformation error: %d\n", GetLastError());
						}
					}
					WTSFreeMemory(pSessionInfo);
				} else {
					TRACE("WTSEnumerateSessions error: %d\n", GetLastError());
				}
				WTSCloseServer(hServer);
			} else {
				TRACE("WTSOpenServer error: %d\n", GetLastError());
			}
		}
		if (!activeSessionCount) {
			if (!hasPreviousSession) {
				launchItem->addValidAddress(address);
			}
		} else {
			launchItem->addFailedAddress(address);
		}
		socket.Close();
	} else {
		TRACE("Cannot connect to address %s : %d\n", address, GetLastError());
		launchItem->addFailedAddress(address);
	}
	return 0;
}

VOID LaunchItem::TimerProc(HWND hwnd,UINT uMsg, UINT_PTR idEvent,DWORD dwTime)
{
	if (idEvent != 0) {
		KillTimer(NULL, idEvent);
	}

}

const char* LaunchItem::GetSelectedAddress()
{
	CSingleLock lock(&AddressesLock, TRUE);
	if (!m_selectedAddress.empty()) {
		return m_selectedAddress.c_str();
	} else if (m_validAddresses.size() > 0) {
		m_selectedAddress = m_validAddresses.front();
		return m_selectedAddress.c_str();
	} else {
		return NULL;
	}
}

void LaunchItem::addFailedAddress(const char* address)
{
	CSingleLock lock(&AddressesLock, TRUE);
	m_failedAddresses.push_back(address);
	if (m_failedAddresses.size() == m_addresses.size()) {
		m_statusMessage = CT2CA(_T("未能找到任何可用云主机。"), CP_UTF8);
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITMEFAILED, 0, (LPARAM)this);
	}
}

void LaunchItem::addValidAddress(const char* address)
{
	CSingleLock lock(&AddressesLock, TRUE);
	m_validAddresses.push_back(address);
}

bool LaunchItem::setSelectedAddress(const char* address)
{
	CSingleLock lock(&AddressesLock, TRUE);
	if (m_selectedAddress.empty()) {
		m_selectedAddress = address;
		TRACE("Selected address %s", address);
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITMECONNECT, 0, (LPARAM)this);
		return TRUE;
	} else {
		TRACE("Already selected address %s, so ignore address %s", m_selectedAddress.c_str(), address);
		return FALSE;
	}

}


void LaunchItem::Disconect()
{
	if (m_remoteWindow) {
		::PostMessage(m_remoteWindow->m_hWnd, WM_CLOSE, NULL, NULL);
		m_remoteWindow = NULL;
	}
	m_status = FREE;
}
