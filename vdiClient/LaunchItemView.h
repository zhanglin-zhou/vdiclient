#pragma once
#include "GridCtrl/GridCell.h"

class LaunchItemView :
	public CGridCell
{
	DECLARE_DYNCREATE(LaunchItemView)
public:
	LaunchItemView(void);
	~LaunchItemView(void);
	virtual BOOL GetTextRect(LPRECT pRect);

	virtual DWORD GetFormat() const;

public:
	static int GetWidth() { return 150; }
	static int GetHeight() { return 180; }

	virtual BOOL Draw(CDC* pDC, int nRow, int nCol, CRect rect, BOOL bEraseBkgnd = TRUE);

	virtual void OnDblClick(CPoint PointCellRelative);

};


