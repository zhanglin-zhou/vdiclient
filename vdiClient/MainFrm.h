// MainFrm.h : interface of the CMainFrame class
//


#pragma once

#include "BrokerView.h"
#include "SelectorView.h"
#include "Broker.h"
#include "BusyView.h"

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
public:

// Operations
public:
	afx_msg LRESULT OnAuthenticate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBrokerConnect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBrokerDisconnected(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBusy(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLaunchItemUpdated(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLaunchItemClicked(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLaunchItemConnect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLaunchItemFailed(WPARAM wParam, LPARAM lParam);
	// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
private:
	Broker *m_currentBroker;
	CWnd *m_currentView;
	CBrokerView *m_brokerView;
	CBusyView *m_busyView;
	CSelectorView *m_selectorView;
	BOOL m_isLogin;
	void SetBusyText(LPCTSTR text);
// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDisconnect();
	afx_msg void OnUpdateDisconnect(CCmdUI *pCmdUI);
	afx_msg void OnUpdateAppExit(CCmdUI *pCmdUI);
	afx_msg void OnClose();
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
//	afx_msg LRESULT OnNcHitTest(CPoint point);
//	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
//	afx_msg void OnMove(int x, int y);
	afx_msg void OnMoving(UINT fwSide, LPRECT pRect);
//	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
//	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnSessionChange(UINT nSessionState, UINT nId);
	afx_msg void OnDestroy();
};


