#pragma once
#include "Broker.h"

// CLoginDialog dialog

class CLoginDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CLoginDialog)

public:
	CLoginDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLoginDialog();
	void SetBroker(Broker *broker);
	INT_PTR DoLock();
// Dialog Data
	enum { IDD = IDD_DLG_LOGIN };

protected:
	virtual	BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	DECLARE_MESSAGE_MAP()

	virtual void OnCancel();

private:
	Broker *m_broker;
	bool m_isLocking;
	CComboBox *m_domainCtrl;
	CEdit *m_usernameCtrl;
	CEdit *m_passwdCtrl;
	CStatic *m_msgCtrl;
};
