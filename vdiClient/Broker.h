#pragma once
#include "LaunchItem.h"
#include <afxmt.h>
//#include <vector>
#include <map>
using namespace std;
class Broker
{
public:
	Broker(const char* url, BOOL isNew=FALSE);
	~Broker(void);
	CCriticalSection launchItemsLock;
	std::map<std::string, LaunchItem*> launchItems;
private:
private:
	string m_name;
	vector<string> m_domains;
	string m_loginDomain;
	string m_username;
	string m_password;
	string m_msg;
	string m_session;
	string m_baseUrl;
	string m_autoConnect;
	LaunchItem *m_defaultLaunchItem;
	BOOL m_isNew;
private:
	CWinThread *m_worker;
	CEvent *m_workerEvent;
	static UINT worker(LPVOID data);
	long updateLaunchItemList(void);
public:
	int authenticate(const char *domain, const char* username, const char* password);
	bool hasSession();
	const char* GetName() { return m_name.c_str();}
	void SetName(const char* name) { m_name = name;}
	const char* GetLoginDomain() { return m_loginDomain.c_str();}
	const char* GetUserName() { return m_username.c_str();}
	const char* GetPassword() { return m_password.c_str();}
	const char* GetErrorMessage() { return m_msg.c_str();}
	const char* GetUrl() { return m_baseUrl.c_str();}
	int GetDomainCount() { return m_domains.size();}
	void SetAutoConnect(const char* autoConnect) { m_autoConnect = autoConnect;}
	const char* GetDomain(int index) { return m_domains[index].c_str();}
	LaunchItem* GetDefaultLaunchItem() { return m_defaultLaunchItem;}
	BOOL isNew() { return m_isNew;}
	BOOL hasConnectedLaunchItem();
public:
	int GetLaunchItemCount() { return launchItems.size();}
	//LaunchItem* GetLaunchItem(int index) { return m_launchItems[index];}
	int Launch(void);
	int Disconnect();
	void Refresh();
};

