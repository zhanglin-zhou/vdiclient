#include "stdafx.h"
#include "Broker.h"
#include "BasicHttp.h"
#include <curl/curl.h>
#include <json/json.h>
#include "Error.h"
#include "Message.h"
#include <afxinet.h>

Broker::Broker(const char* url, BOOL isNew)
	: m_name(""),
	  m_defaultLaunchItem(NULL),
	  m_session(""),
	  m_loginDomain(""),
	  m_username(""),
	  m_password(""),
	  m_baseUrl(url)
{
	m_isNew = isNew;
}


Broker::~Broker(void)
{
	Disconnect();
	for (std::map<std::string, LaunchItem*>::iterator it = launchItems.begin(); it != launchItems.end(); ++it) {
		delete it->second;
	}

}


int Broker::authenticate(const char *domain, const char* username, const char* password)
{
	BasicHttpRequest request;
	BOOL result = FALSE;
	request.setUrl((m_baseUrl + "/login").c_str());
	string body("domain=");
	body += domain;
	body += "&username=";
	body += username;
	body += "&password=";
	body += password; 
	request.setBody(body.c_str());
	request.doPost();
	if (request.getResponseCode() == 200) {
		Json::Reader reader;
		Json::Value root;
		TRACE("Get response: %s", request.getResponse());
		if (reader.parse(request.getResponse(), root)) {
			string status = root["status"].asString();
			if (status.compare("success") == 0) {
				m_loginDomain = domain;
				m_username = username;
				m_password = password;
				m_session = request.getCookie();
				DWORD serviceType;
				CString server;
				CString relPath;
				INTERNET_PORT port;
				if (AfxParseURL(CA2CT(m_baseUrl.c_str()), serviceType, server, relPath, port)) {
					m_name = CT2CA(server, CP_UTF8);
				}
				result = TRUE;
			} else {
				m_msg = root["msg"].asString();
			}
		}
	} else {
		TRACE("Authenticate error: %d\n", request.getResponseCode());
	}
	return result;
}

bool Broker::hasSession()
{
	return m_session.size() > 0;
}


long Broker::updateLaunchItemList(void)
{
#if 0
	if (!hasSession()) {
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_AUTHENTICATE, 0, (LPARAM) this);

		/* ************************************************************************ */
		request.setUrl(CT2CA(m_baseUrl + _T("/login"), CP_UTF8));
		string body("");
		body += "domain=" + m_domain + "&username=" + m_username + "&password=" + m_password;
		request.setBody(body.c_str());
		request.doPost();
		/* ************************************************************************ */

	} else 
#endif
	{
		BasicHttpRequest request;
		request.setCookie(m_session.c_str());
		request.setUrl((m_baseUrl + "/launchItems").c_str());
		int ret = request.doGet();
		if (ret != 0) {
			::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_BROKERDISCONNECTED, ret, (LPARAM) this);
			return ret;
		}
		Json::Reader reader;
		Json::Value root;
		if (request.getResponseCode() == 401) {
			m_session.clear();
			if (reader.parse(request.getResponse(), root)) {
				m_baseUrl = root["base"].asString();
				if (root["domains"].isArray()) {
					int cnt = root["domains"].size();
					for (int i = 0; i < cnt; i++) {
						m_domains.push_back(root["domains"][i].asString());
					}
				}
				::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_AUTHENTICATE, 0, (LPARAM) this);
			}
		} else {
			if (reader.parse(request.getResponse(), root)) {
				if (root.isArray()) {
					CSingleLock lock(&launchItemsLock, TRUE);
					std::map<std::string, LaunchItem*> updateLaunchItems;
					int cnt = root.size();
					for (int i = 0; i < cnt; i++) {
						string id = root[i]["id"].asString();
						LaunchItem *launchItem = NULL;
						if (launchItems.find(id) != launchItems.end()) {
							launchItem = launchItems[id];
							launchItems.erase(id);
						} else {
							launchItem = new LaunchItem(this);
							launchItem->SetId(id.c_str());
						}
						launchItem->SetName(root[i]["name"].asString().c_str());
						if (m_autoConnect == launchItem->GetName()) {
							m_defaultLaunchItem = launchItem;
						}
						launchItem->ClearAddress();
						Json::Value addresses = root[i]["address"];
						if (addresses.isArray()) {
							for (int j = 0; j < addresses.size(); j++) {
								launchItem->addAddress(addresses[j].asString().c_str());
							}
							//TODO: remove
							//launchItem->addAddress("10.112.117.125");
						}
						updateLaunchItems[id] = launchItem;
					}
					for (std::map<std::string, LaunchItem*>::iterator it = launchItems.begin(); it != launchItems.end(); it++) {
						delete it->second;
					}
					launchItems.swap(updateLaunchItems);
					if (!m_defaultLaunchItem && launchItems.size() == 1) {
						m_defaultLaunchItem = launchItems.begin()->second;
					}
					::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_LAUNCHITEMUPDATED, 0, (LPARAM)this);
				} else {
					TRACE("Get error response");
				}
			}
		}
	}

	return 0;
}


int Broker::Launch(void)
{
	//m_workerEvent = new CEvent(FALSE, FALSE);
	// refresh to get the launch item list
	Refresh();
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_BROKERCONNECT, 0, (LPARAM)this);
	return 0;
}



UINT Broker::worker(LPVOID data)
{
	Broker *broker = (Broker*) data;
#if 0
	while (::WaitForSingleObject(broker->m_workerEvent->m_hObject, 2000) != WAIT_OBJECT_0) {
		broker->updateLaunchItemList();
	}
#endif
	broker->updateLaunchItemList();
	return 0;
}


int Broker::Disconnect()
{
#if 0
	if (m_worker) {
		m_workerEvent->SetEvent();
		::WaitForSingleObject(m_worker->m_hThread, 5000);
		delete m_workerEvent;
		delete m_worker;
		m_worker = NULL;
		m_workerEvent = NULL;
	}
#endif
	for (std::map<std::string, LaunchItem*>::iterator it = launchItems.begin(); it != launchItems.end(); ++it) {
		it->second->Disconect();
	}
	m_session.clear();
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_BROKERDISCONNECTED, 0, (LPARAM)this);
	return 0;
}

void Broker::Refresh()
{
	CWinThread *workerThread = ::AfxBeginThread(worker, this);
	workerThread->m_bAutoDelete = TRUE;
}

BOOL Broker::hasConnectedLaunchItem()
{
	for (std::map<std::string, LaunchItem*>::iterator it = launchItems.begin(); it != launchItems.end(); ++it) {
		if (it->second->GetStatus() != FREE) {
			return TRUE;
		}
	}
	return FALSE;
}