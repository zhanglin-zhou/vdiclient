// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Windows\\System32\\mstscax.dll" no_namespace
// CMsTscAxEvents wrapper class

class CMsTscAxEvents : public COleDispatchDriver
{
public:
	CMsTscAxEvents(){} // Calls COleDispatchDriver default constructor
	CMsTscAxEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CMsTscAxEvents(const CMsTscAxEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IMsTscAxEvents methods
public:
	void OnConnecting()
	{
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnConnected()
	{
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnLoginComplete()
	{
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnDisconnected(long discReason)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms, discReason);
	}
	void OnEnterFullScreenMode()
	{
		InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnLeaveFullScreenMode()
	{
		InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnChannelReceivedData(LPCTSTR chanName, LPCTSTR data)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms, chanName, data);
	}
	void OnRequestGoFullScreen()
	{
		InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnRequestLeaveFullScreen()
	{
		InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnFatalError(long errorCode)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms, errorCode);
	}
	void OnWarning(long warningCode)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms, warningCode);
	}
	void OnRemoteDesktopSizeChange(long width, long height)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms, width, height);
	}
	void OnIdleTimeoutNotification()
	{
		InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnRequestContainerMinimize()
	{
		InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnConfirmClose(BOOL * pfAllowClose)
	{
		static BYTE parms[] = VTS_PBOOL ;
		InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, parms, pfAllowClose);
	}
	void OnReceivedTSPublicKey(LPCTSTR publicKey, BOOL * pfContinueLogon)
	{
		static BYTE parms[] = VTS_BSTR VTS_PBOOL ;
		InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms, publicKey, pfContinueLogon);
	}
	void OnAutoReconnecting(long disconnectReason, long attemptCount, long * pArcContinueStatus)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_PI4 ;
		InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms, disconnectReason, attemptCount, pArcContinueStatus);
	}
	void OnAuthenticationWarningDisplayed()
	{
		InvokeHelper(0x12, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnAuthenticationWarningDismissed()
	{
		InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnRemoteProgramResult(LPCTSTR bstrRemoteProgram, long lError, BOOL vbIsExecutable)
	{
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_BOOL ;
		InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, parms, bstrRemoteProgram, lError, vbIsExecutable);
	}
	void OnRemoteProgramDisplayed(BOOL vbDisplayed, unsigned long uDisplayInformation)
	{
		static BYTE parms[] = VTS_BOOL VTS_UI4 ;
		InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms, vbDisplayed, uDisplayInformation);
	}
	void OnLogonError(long lError)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms, lError);
	}
	void OnFocusReleased(long iDirection)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, parms, iDirection);
	}
	void OnUserNameAcquired(LPCTSTR bstrUserName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, parms, bstrUserName);
	}
	void OnMouseInputModeChanged(BOOL fMouseModeRelative)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fMouseModeRelative);
	}
	void OnServiceMessageRecieved(LPCTSTR serviceMessage)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x1c, DISPATCH_METHOD, VT_EMPTY, NULL, parms, serviceMessage);
	}

	// IMsTscAxEvents properties
public:

};
