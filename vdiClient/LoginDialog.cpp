﻿// LoginDialog.cpp : implementation file
//

#include "stdafx.h"
#include "vdiClient.h"
#include "LoginDialog.h"
#include "afxdialogex.h"
#include "Message.h"


// CLoginDialog dialog

IMPLEMENT_DYNAMIC(CLoginDialog, CDialogEx)

CLoginDialog::CLoginDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLoginDialog::IDD, pParent),
	  m_isLocking(FALSE),
	  m_broker(NULL)
{

}

CLoginDialog::~CLoginDialog()
{
}

void CLoginDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLoginDialog, CDialogEx)
//	ON_WM_CREATE()
//	ON_BN_CLICKED(IDOK, &CLoginDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// CLoginDialog message handlers


//int CLoginDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
//{
//	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
//		return -1;
//
//		// TODO:  Add your specialized creation code here
//	
//
//		return 0;
//}

BOOL CLoginDialog::OnInitDialog() 
{
	m_domainCtrl = (CComboBox*)GetDlgItem(IDC_COMBO1);
	m_usernameCtrl = (CEdit *) GetDlgItem(IDC_EDIT1);
	m_passwdCtrl = (CEdit *) GetDlgItem(IDC_EDIT2);
	m_msgCtrl = (CStatic *) GetDlgItem(IDC_LOGIN_MSG);
	m_passwdCtrl->SetPasswordChar('*');
	m_domainCtrl->ResetContent();
	int cnt = m_broker->GetDomainCount();
	for (int i = 0; i < cnt; i++) {
		m_domainCtrl->AddString(CA2CT(m_broker->GetDomain(i)));
	}
	if (m_isLocking) {
		m_domainCtrl->SelectString(-1, CA2CT(m_broker->GetLoginDomain(), CP_UTF8));
		m_domainCtrl->EnableWindow(FALSE);
		m_usernameCtrl->SetWindowText(CA2CT(m_broker->GetUserName(), CP_UTF8));
		m_usernameCtrl->EnableWindow(FALSE);
	} else {
		m_domainCtrl->SelectString(-1, CA2CT(m_broker->GetDomain(0)));
	}

	CStatic* pWnd = (CStatic*)GetDlgItem(IDC_DLG_IMAGE);
	RECT rect;
	pWnd->GetClientRect(&rect);
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;
	CPngImage pngImage;
	pngImage.Load(IDB_PNG7);

	CImage image;
	image.Attach((HBITMAP)pngImage.Detach());
	CDC *pDC = pWnd->GetDC();
	CDC mDC;
	mDC.CreateCompatibleDC(pDC);
	CBitmap n;
	n.CreateCompatibleBitmap(pDC, width, height);

	CBitmap *pOld = mDC.SelectObject(&n);
	mDC.SetStretchBltMode(COLORONCOLOR);
	image.StretchBlt(mDC.m_hDC, 0, 0, width, height, SRCCOPY);
	mDC.SelectObject(pOld);
	pWnd->SetBitmap((HBITMAP)n.Detach());
	return CDialogEx::OnInitDialog();
}

void CLoginDialog::SetBroker(Broker* broker)
{
	m_broker = broker;
}

void CLoginDialog::OnOK()
{
	CString domain;
	CString username;
	CString password;
	m_domainCtrl->GetWindowTextW(domain);
	m_usernameCtrl->GetWindowTextW(username);
	m_passwdCtrl->GetWindowTextW(password);
	if (m_isLocking) {
		if (password.Compare(CA2CT(m_broker->GetPassword(), CP_UTF8)) == 0) {
			CDialogEx::OnOK();
		} else {
			m_msgCtrl->SetWindowTextW(_T("密码错误"));
		}
	} else {
		if (m_broker->authenticate(CT2CA(domain, CP_UTF8), CT2CA(username, CP_UTF8), CT2CA(password, CP_UTF8))) {
			CDialogEx::OnOK();
			m_broker->Refresh();
		} else {
			m_msgCtrl->SetWindowTextW(CA2CT(m_broker->GetErrorMessage(), CP_UTF8));
		}
	}
}

void CLoginDialog::OnCancel()
{
	m_broker->Disconnect();
	return CDialogEx::OnCancel();
}

INT_PTR CLoginDialog::DoLock()
{
	m_isLocking = TRUE;
	return DoModal();
}