#pragma once
#include "GridCtrl/GridCtrl.h"
#include "Broker.h"
#include "RDPRemoteFrame.h"
#include <vector>
using namespace std;
// CSelectorView

class CSelectorView : public CWnd
{
	DECLARE_DYNAMIC(CSelectorView)

public:
	CSelectorView();
	virtual ~CSelectorView();

protected:
	DECLARE_MESSAGE_MAP()
public:
	void SetBroker(Broker* broker) { m_broker = broker;}
	bool Launch(LaunchItem *data);
	void Lock();
	void Connect(LaunchItem *launchItem);
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
private:
	void LoadImage();
	CGridCtrl m_grid;
	Broker *m_broker;
	int m_maxColumn;
	int m_column;
	int m_row;
	CImageList m_imageList;
	bool m_locked;
public:
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	void Refresh();
};


